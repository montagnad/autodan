@extends('adminlte::page')

@section('content')
    <h3>Nova Versão</h3>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::open(['route'=>'versoes.store']) !!}
        <div class="form-group">
            {!! Form::label('nome', 'Nome:') !!}
            {!! Form::text('nome', null, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('motor_id', 'Motor:') !!}
            {!! Form::select('motor_id', 
                            \App\Models\Motor::orderBy('nome')->pluck('nome', 'id')->toArray(), 
                            null, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('modelo_id', 'Modelo:') !!}
            {!! Form::select('modelo_id', 
                            \App\Models\Modelo::orderBy('nome')->pluck('nome', 'id')->toArray(), 
                            null, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Criar Versão', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar',['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop