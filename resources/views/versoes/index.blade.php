@extends('layouts.default')

@section('content')
    <h1>Versões<option value=""></option></h1>

    {!! Form::open(['name'=>'form_name', 'route'=>'versoes']) !!}

    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Motor</th>
            <th>Modelo</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($versoes as $versao)
                <tr>
                    <td>{{ $versao->nome }}</td>
                    <td>{{ $versao->motor->nome }}</td>
                    <td>{{ $versao->modelo->nome }}</td>
                    <td>
                        <a href="{{ route('versoes.edit', ['id'=>$versao->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmarExclusao({{$versao->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>    
            @endforeach
        </tbody>
    </table>

    {{ $versoes->links("pagination::bootstrap-4") }}

    <a href="{{ route('versoes.create', []) }}" class="btn-sm btn-info">Adicionar</a>
@stop

@section('table-delete')
"versoes"
@endsection