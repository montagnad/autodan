@extends('layouts.default')

@section('content')
    <h1>Marcas</h1>
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Nacionalidade</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($marcas as $marca)
            <tr>
                <td>{{$marca->nome}}</td>
                <td>{{$marca->nacionalidade}}</td>
                <td>
                    <a href="{{ route('marcas.edit', ['id'=>$marca->id]) }}" class="btn-sm btn-success">Editar</a>
                    <a href="#" onclick="return ConfirmarExclusao({{$marca->id}})" class="btn-sm btn-danger">Remover</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@stop
@section('table-delete')
"marcas"
@endsection