@extends('layouts.default')

@section('content')
    <div>
        <h3>Modelos</h3>
        <table class="table table-stripe table-bordered table-hover">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Marca</th>
                    <th>Categorias</th>
                </tr>
                
            </thead>
            <tbody>
                @foreach($modelos as $modelo)
                    <tr>
                        <td>{{ $modelo->nome }}</td>
                        <td>{{ $modelo->marca->nome }}</td>
                        <td>
                            @foreach ($modelo->categorias as $c)
                                <li>{{ $c->categoria->nome }}</li>
                            @endforeach
                        </td>
                    </tr>    
                @endforeach
            </tbody>
        </table>

        <a href="{{ route('modelos.create', []) }}" class="btn-sm btn-info">Adicionar</a>
    </div>
    
@stop