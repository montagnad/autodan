@extends('layouts.default')

@section('content')
    <h1>Motores</h1>
    <table class="table table-stripe table-bordered table-hover">
        <thead>
            <th>Nome</th>
            <th>Torque</th>
            <th>Potencia</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($motores as $motor)
                <tr>
                    <td>{{ $motor->nome }}</td>
                    <td>{{ $motor->torque }}</td>
                    <td>{{ $motor->potencia }}</td>
                    <td>
                        <a href="{{ route('motores.edit', ['id'=>$motor->id]) }}" class="btn-sm btn-success">Editar</a>
                        <a href="#" onclick="return ConfirmarExclusao({{$motor->id}})" class="btn-sm btn-danger">Remover</a>
                    </td>
                </tr>    
            @endforeach
        </tbody>
    </table>

    {{ $motores->links("pagination::bootstrap-4") }}

    <a href="{{ route('motores.create', []) }}" class="btn-sm btn-info">Adicionar</a>
@stop

@section('table-delete')
"motores"
@endsection