@extends('adminlte::page')

@section('content')
    <h3>Editando Motor: {{ $motor->nome }}</h3>

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::open(['route'=> ["motores.update", 'id'=>$motor->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('nome', 'Nome:') !!}
            {!! Form::text('nome', $motor->nome, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('torque', 'Torque:') !!}
            {!! Form::text('torque', $motor->torque, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('potencia', 'Potencia:') !!}
            {!! Form::text('potencia', $motor->potencia, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Editar Motr', ['class'=>'btn btn-primary']) !!}
            {!! Form::reset('Limpar',['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@stop