<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function() {
    Route::group(['prefix'=>'marcas', 'where'=>['id'=>'[0-9]+']], function(){
        Route::get('',              ['as' => 'marcas',           'uses' => 'App\Http\Controllers\MarcasController@index'     ]);
        Route::get('create',        ['as' => 'marcas.create',    'uses' => 'App\Http\Controllers\MarcasController@create'    ]);
        Route::post('store',        ['as' => 'marcas.store',     'uses' => 'App\Http\Controllers\MarcasController@store'     ]);
        Route::get('{id}/destroy',  ['as' => 'marcas.destroy',   'uses' => 'App\Http\Controllers\MarcasController@destroy'   ]);
        Route::get('{id}/edit',     ['as' => 'marcas.edit',      'uses' => 'App\Http\Controllers\MarcasController@edit'      ]);
        Route::put('{id}/update',   ['as' => 'marcas.update',    'uses' => 'App\Http\Controllers\MarcasController@update'    ]);
    });

    Route::group(['prefix'=>'motores', 'where'=>['id'=>'[0-9]+']], function(){
        Route::get('',              ['as' => 'motores',           'uses' => 'App\Http\Controllers\MotoresController@index'     ]);
        Route::get('create',        ['as' => 'motores.create',    'uses' => 'App\Http\Controllers\MotoresController@create'    ]);
        Route::post('store',        ['as' => 'motores.store',     'uses' => 'App\Http\Controllers\MotoresController@store'     ]);
        Route::get('{id}/destroy',  ['as' => 'motores.destroy',   'uses' => 'App\Http\Controllers\MotoresController@destroy'   ]);
        Route::get('{id}/edit',     ['as' => 'motores.edit',      'uses' => 'App\Http\Controllers\MotoresController@edit'      ]);
        Route::put('{id}/update',   ['as' => 'motores.update',    'uses' => 'App\Http\Controllers\MotoresController@update'    ]);
    });

    Route::group(['prefix'=>'categorias', 'where'=>['id'=>'[0-9]+']], function(){
        Route::any('',              ['as' => 'categorias',           'uses' => 'App\Http\Controllers\CategoriasController@index'     ]);
        Route::get('create',        ['as' => 'categorias.create',    'uses' => 'App\Http\Controllers\CategoriasController@create'    ]);
        Route::post('store',        ['as' => 'categorias.store',     'uses' => 'App\Http\Controllers\CategoriasController@store'     ]);
        Route::get('{id}/destroy',  ['as' => 'categorias.destroy',   'uses' => 'App\Http\Controllers\CategoriasController@destroy'   ]);
        Route::get('{id}/edit',     ['as' => 'categorias.edit',      'uses' => 'App\Http\Controllers\CategoriasController@edit'      ]);
        Route::put('{id}/update',   ['as' => 'categorias.update',    'uses' => 'App\Http\Controllers\CategoriasController@update'    ]);
    });

    Route::group(['prefix'=>'versoes', 'where'=>['id'=>'[0-9]+']], function(){
        Route::get('',              ['as' => 'versoes',           'uses' => 'App\Http\Controllers\VersoesController@index'     ]);
        Route::get('create',        ['as' => 'versoes.create',    'uses' => 'App\Http\Controllers\VersoesController@create'    ]);
        Route::post('store',        ['as' => 'versoes.store',     'uses' => 'App\Http\Controllers\VersoesController@store'     ]);
        Route::get('{id}/destroy',  ['as' => 'versoes.destroy',   'uses' => 'App\Http\Controllers\VersoesController@destroy'   ]);
        Route::get('{id}/edit',     ['as' => 'versoes.edit',      'uses' => 'App\Http\Controllers\VersoesController@edit'      ]);
        Route::put('{id}/update',   ['as' => 'versoes.update',    'uses' => 'App\Http\Controllers\VersoesController@update'    ]);
    });

    Route::group(['prefix'=>'modelos', 'where'=>['id'=>'[0-9]+']], function(){
        Route::get('',             ['as' => 'modelos',           'uses' => 'App\Http\Controllers\ModelosController@index'    ]);
        Route::get('create',       ['as' => 'modelos.create',    'uses' => 'App\Http\Controllers\ModelosController@create'   ]);
        Route::post('store',       ['as' => 'modelos.store',     'uses' => 'App\Http\Controllers\ModelosController@store'    ]);
    });
});




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
