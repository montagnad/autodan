<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    use HasFactory;
    protected $table = "modelos";
    protected $fillable = ['nome', 'marca_id'];

    public function versoes() {
        return $this->hasMany("App\Models\Versao;");
    }

    public function marca() {
        return $this->belongsTo("App\Models\Marca");
    }

    public function categorias() {
        return $this->hasMany("App\Models\CategoriaModelo");
    }
}
