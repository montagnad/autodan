<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Versao extends Model
{
    use HasFactory;
    protected $table = "versaos";
    protected $fillable = ['nome', 'motor_id', 'modelo_id'];

    public function motor() {
        return $this->belongsTo("App\Models\Motor");
    }

    public function modelo() {
        return $this->belongsTo("App\Models\Modelo");
    }

    
}
