<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriaModelo extends Model
{
    use HasFactory;

    protected $table = "categoria_modelos";
    protected $fillable = ['modelo_id', 'categoria_id'];

    public function categoria() {
        return $this->belongsTo("App\Models\Categoria");
    }
}
