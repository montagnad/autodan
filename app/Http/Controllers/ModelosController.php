<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Modelo;
use App\Models\CategoriaModelo;

class ModelosController extends Controller
{
    public function index() {
        $modelos = Modelo::all();
        return view('modelos.index', ['modelos'=>$modelos]);
    }

    public function create() {
        return view('modelos.create');
    }

    public function store(Request $request) {
        $modelo = Modelo::create([
            'nome' => $request->get('nome'),
            'marca_id' => $request->get('marca_id')
        ]);
        $categorias = $request->categorias;
        foreach($categorias as $c => $value) {
            CategoriaModelo::create([
                'modelo_id' => $modelo->id,
                'categoria_id' => $categorias[$c]
            ]);
        }

        return redirect()->route('modelos');
    }
}
