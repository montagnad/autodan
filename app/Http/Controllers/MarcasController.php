<?php

namespace App\Http\Controllers;

use App\Http\Requests\MarcaRequest;
use Illuminate\Http\Request;
use App\Models\Marca;

class MarcasController extends Controller
{
    public function index() {
        $marca = Marca::All();
        return view('marcas.index', ['marcas'=>$marca]);
    }

    public function create(){
        return view('marcas.create');
    }

    public function store(MarcaRequest $request) {
        $nova_marca = $request->all();
        Marca::create($nova_marca);

        return redirect('marcas');
    }

    public function destroy($id) {
        Marca::find($id)->delete();

        return redirect('marcas');
    }

    public function edit($id) {
        $marca = Marca::find($id);

        return view('marcas.edit', compact('marca'));
    }

    public function update(MarcaRequest $request, $id) {
        Marca::find($id)->update($request->all());

        return redirect('marcas');
    }
}