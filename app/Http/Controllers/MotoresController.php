<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Motor;

class MotoresController extends Controller
{
    public function index() {
        $motor = Motor::orderBy('nome')->paginate(5);
        return view('motores.index', ['motores'=>$motor]);
    }

    public function create() {
        return view('motores.create');
    }

    public function store(Request $request){
        $novo_motor = $request->all();
        Motor::create($novo_motor);

        return redirect()->route('motores');
    }

    public function destroy($id) {
        try{ 
            Motor::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>'null');
        } catch (\Illuminate\Database\QueryException $e) {
            $ret = array('status'=>500, 'msg'=> $e->getMessage());
        }catch(\PDOException $e) {
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        return $ret;
    }

    public function edit($id) {
        $motor = Motor::find($id);
        return view('motores.edit', compact('motor'));
    }

    public function update(Request $request, $id){
        Motor::find($id)->update($request->all());
        return redirect()->route('motores');
    }
}
