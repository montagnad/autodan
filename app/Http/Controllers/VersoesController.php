<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Versao;

class VersoesController extends Controller
{
    public function index() {
        $versao = Versao::orderBy('nome')->paginate(5);
        return view('versoes.index', ['versoes'=>$versao]);
    }

    public function create() {
        return view('versoes.create');
    }

    public function store(Request $request){
        $nova_versao = $request->all();
        Versao::create($nova_versao);

        return redirect()->route('versoes');
    }

    public function destroy($id) {
        try{ 
            Versao::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>'null');
        } catch (\Illuminate\Database\QueryException $e) {
            $ret = array('status'=>500, 'msg'=> $e->getMessage());
        }catch(\PDOException $e) {
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        return $ret;
    }

    public function edit($id) {
        $versao = Versao::find($id);
        return view('versoes.edit', compact('versao'));
    }

    public function update(Request $request, $id){
        Versao::find($id)->update($request->all());
        return redirect()->route('versoes');
    }
}
