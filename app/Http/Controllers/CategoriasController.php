<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;

class CategoriasController extends Controller
{

    public function index(Request $filtro) {
        $filtragem = $filtro->get('desc_filtro');
        if($filtragem == null)
            $categorias = Categoria::orderBy('nome')->paginate(5);
        else
            $categorias = Categoria::where('nome', 'like', '%'.$filtragem.'%')
                                ->orderBy("nome")
                                ->paginate(5)
                                ->setPath('categorias?desc_filtro='.$filtragem);

        return view('categorias.index', ['categorias'=>$categorias]);
    }

    public function create() {
        return view('categorias.create');
    }

    public function store(Request $request){
        $nova_categoria = $request->all();
        Categoria::create($nova_categoria);

        return redirect()->route('categorias');
    }

    public function destroy($id) {
        try{ 
            Categoria::find($id)->delete();
            $ret = array('status'=>200, 'msg'=>'null');
        } catch (\Illuminate\Database\QueryException $e) {
            $ret = array('status'=>500, 'msg'=> $e->getMessage());
        }catch(\PDOException $e) {
            $ret = array('status'=>500, 'msg'=>$e->getMessage());
        }
        return $ret;
    }

    public function edit($id) {
        $categoria = Categoria::find($id);
        return view('categorias.edit', compact('categoria'));
    }

    public function update(Request $request, $id){
        Categoria::find($id)->update($request->all());
        return redirect()->route('categorias');
    }
}
